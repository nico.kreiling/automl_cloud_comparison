from abc import ABCMeta, abstractmethod

class AutoML(object, metaclass=ABCMeta):
        
    @abstractmethod
    def __init__(
        self,
        project_name: str,
        target_col: str,
        problem_type: str,
        max_training_time_min: int,
        bucket_name: str,
        region: str
    ):
        self.bucket_name = bucket_name
        self.region = region
        self.project_name = project_name
        self.target_col = target_col
        self.problem_type = problem_type
        if (max_training_time_min < 20) or (max_training_time_min>300):
            raise Exception("You defined an unusual training time")
        self.max_training_time_min = max_training_time_min

    @abstractmethod
    def prepare(self):
        pass

    @abstractmethod
    def upload_data(self, src_path: str, target_path: str):
        pass

    @abstractmethod
    def train_model(self):
        pass

    @abstractmethod
    def describe_model(self):
        pass

    @abstractmethod
    def get_state(self):
        pass

    @abstractmethod
    def get_results(self):
        pass

    @abstractmethod
    def tear_down(self):
        pass