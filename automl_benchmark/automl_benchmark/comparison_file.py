
from abc import abstractmethod

class ComparisonClass:
    @abstractmethod
    def create_dataset(self, input_csv_file, target_col, display_name= None):
        pass

    @abstractmethod
    def get_dataset(self, dataset_name):
        pass

    @abstractmethod
    def describe_dataset(self, dataset=None):
        pass

    @abstractmethod
    def train_model(self, train_hours = 1, exclude_columns=[]):
        pass
