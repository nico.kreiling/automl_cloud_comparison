import logging

from azureml.core.compute import AmlCompute, ComputeTarget
from azureml.core.compute_target import ComputeTargetException
from azureml.core.dataset import Dataset
from azureml.core.experiment import Experiment
from azureml.train.automl.run import AutoMLRun
from azureml.core.workspace import Workspace
from azureml.explain.model._internal.explanation_client import \
    ExplanationClient
from azureml.train.automl import AutoMLConfig

from .auto_ml import AutoML
from .comparison_file import ComparisonClass
from typing import Optional
from pathlib import Path


problem_type_mapping = dict()
problem_type_mapping["BinaryClassification"] = 'classification'
problem_type_mapping["MulticlassClassification"] = 'classification'
problem_type_mapping["Regression"] = 'regression'

target_metrics = dict()
# precision_score_weighted, average_precision_score_weighted, AUC_weighted, norm_macro_recall, accuracy
target_metrics["BinaryClassification"] = 'AUC_weighted'
target_metrics["MulticlassClassification"] = 'AUC_weighted'
# r2_score, normalized_mean_absolute_error, spearman_correlation, normalized_root_mean_squared_error
target_metrics["Regression"] = 'normalized_root_mean_squared_error'

class AzureAutoML(AutoML):
    def __init__(
        self, project_defaults, workspace: Workspace
    ):
        """Azure Auto ML

        Args:
            workspace (Workspace): The workspace to use
            project (str): Experiment name
            label_column (str): The column which contains the label
            target_metric (str, optional): One out of (). Defaults to "normalized_mean_absolute_error".
        """
        project_defaults["region"] = None
        project_defaults["bucket_name"] = None
        super().__init__(**project_defaults)

        self.ws = workspace
        self.ds = workspace.get_default_datastore()
        self.compute_target = None
        self.experiment = Experiment(workspace, self.project_name)
        self.data_store = workspace.get_default_datastore()
        self.last_submitted_automl_job: Optional(AutoMLRun) = None
        # self.storage_client = storage.Client(credentials=credentials, project=self.project_id)
        self.dataset = None

    def _create_cluster(
        self, cpu_cluster_name="default", max_nodes=4, vm_size="STANDARD_D2_V2"
    ):
        # Verify that cluster does not exist already
        try:
            compute_target = ComputeTarget(workspace=self.ws, name=cpu_cluster_name)
            print("Found existing cluster, use it.")
        except ComputeTargetException:
            compute_config = AmlCompute.provisioning_configuration(
                vm_size=vm_size, max_nodes=max_nodes
            )
            compute_target = ComputeTarget.create(
                self.ws, cpu_cluster_name, compute_config
            )

        compute_target.wait_for_completion(show_output=True)
        self.compute_target = compute_target

    # def get_dataset(self, dataset_name):
    #     dataset = self.tables_client.get_dataset(
    #         self.project_id, self.region, dataset_display_name=dataset_name
    #     )
    #     self.dataset = dataset
    #     return dataset

    def prepare(
        self, cpu_cluster_name="default", max_nodes=4, vm_size="STANDARD_D2_V2"
    ):
        self._create_cluster(cpu_cluster_name, max_nodes, vm_size)

    def upload_data(self, src_dir, target_path):
        self.ds.upload(
            src_dir=src_dir, target_path=target_path, overwrite=True, show_progress=True
        )
        self.train_data = Dataset.Tabular.from_delimited_files(
            path=self.ds.path(f"{target_path}/train.csv")
        )

        if Path(f"{src_dir}/validation.csv").exists():
            self.validation_data = Dataset.Tabular.from_delimited_files(
                path=self.ds.path(f"{target_path}/validation.csv")
            )


        if Path(f"{src_dir}/test.csv").exists():
            self.validation_data = Dataset.Tabular.from_delimited_files(
                path=self.ds.path(f"{target_path}/test.csv")
            )

    def train_model(self, target_metric=None, use_validation_data=False):

        if target_metric is None:
            target_metric = target_metrics[self.problem_type]

        automl_settings = {
            "experiment_timeout_minutes": self.max_training_time_min,
            "enable_early_stopping": True,
            "iteration_timeout_minutes": 5,
            "max_concurrent_iterations": 4,
            "max_cores_per_iteration": -1,
            "primary_metric": target_metric,
            "featurization": "auto",
            "verbosity": logging.INFO,
        }

        automl_config = AutoMLConfig(
            task=problem_type_mapping[self.problem_type],
            debug_log="automl_regression_errors.log",
            compute_target=self.compute_target,
            training_data=self.train_data,
            label_column_name=self.target_col,
            **automl_settings,
        )

        if use_validation_data:
            auto_ml["validation_data"] = self.validation_data

        remote_run = self.experiment.submit(automl_config, show_output=False)
        self.last_submitted_automl_job = remote_run
        return remote_run

    def get_results(self, remote_run=None):
        if remote_run is None:
            remote_run = self.last_submitted_automl_job

        if remote_run.status != "Completed":
            raise Exception("Run is not completed")
        else:
            best_run_customized, fitted_model_customized = remote_run.get_output()
        return best_run_customized, fitted_model_customized

    def describe_model(self, run):
        client = ExplanationClient.from_run(run)
        engineered_explanations = client.download_model_explanation(raw=False)
        exp_data = engineered_explanations.get_feature_importance_dict()
        return exp_data

    def get_state(self, auto_ml_job_name: str = None):
        if auto_ml_job_name is None:
            auto_ml_job_name = self.last_submitted_automl_job
        return auto_ml_job_name.status

    def tear_down(self):
        pass