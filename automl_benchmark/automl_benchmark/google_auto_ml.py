import os

import google.cloud.automl_v1beta1.proto.data_types_pb2 as data_types
# Imports the Google Cloud client library
from google.cloud import automl_v1beta1 as automl
from google.cloud import storage
from google.cloud.automl_v1beta1.types import ModelExportOutputConfig
import pandas as pd

from .auto_ml import AutoML


class GoogleAutoML(AutoML):
    def __init__(
        self,
        project_defaults, 
        config_path: str = None,
        region="us-central1",
        bucket_name="automl_nkreiling",
        google_project_id="kubeyard",
    ):
        project_defaults["region"] = region
        project_defaults["bucket_name"] = bucket_name
        super().__init__(**project_defaults)
        
        self.bucket_name = bucket_name
        self.region = region
        self.google_project_id = google_project_id
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = config_path

        self.automl_client = automl.AutoMlClient()
        self.tables_client = automl.TablesClient(
            project=google_project_id, region=region
        )
        self.location_path = self.automl_client.location_path(self.google_project_id, region)
        # self.storage_client = storage.Client(credentials=credentials, project=self.google_project_id)
        self.dataset = None

    def _upload_blob(self, source_file_path, destination_blob_name):
        """Uploads a file to the bucket."""
        #  "your-bucket-name"
        # source_file_path = "local/path/to/file"
        # destination_blob_name = "storage-object-name"

        blob = self.bucket.blob(destination_blob_name)
        blob.upload_from_filename(source_file_path)

        print("File {} uploaded to {}.".format(source_file_path, destination_blob_name))

    def _create_dataset(self, input_csv_file: str, display_name: str = None):
        if display_name is None:
            display_name = self.project_name

        print("upload file")
        GCS_DATASET_URI = "gs://{}/{}".format(self.bucket_name, input_csv_file)

        dataset = self.tables_client.create_dataset(dataset_display_name=display_name)
        print("import data")
        import_data_response = self.tables_client.import_data(
            dataset=dataset, gcs_input_uris=GCS_DATASET_URI
        )
        self.dataset = dataset

        import_data_response.result()

        print("set target col")
        update_dataset_response = self.tables_client.set_target_column(
            dataset=self.dataset, column_spec_display_name=self.target_col,
        )

        return update_dataset_response

    def _export_model(self, name):
        result_bucket_path = f"gs://{self.bucket_name}/{self.project_name}/results"

        config = {
            "model_format": "tf_saved_model",
            "gcs_destination": {
                "output_uri_prefix": result_bucket_path
            }
        }
        return self.automl_client.export_model(name, config)

    def _download_model(self, path):
        from pathlib import Path
        from google.cloud.storage import Client
        print(f"start downloading from {path}")

        os.makedirs("results/google", exist_ok=True)

        prefix = path.replace("gs://"+self.bucket_name+"/","")

        blobs = self.bucket.list_blobs(prefix=prefix)  # Get list of files
        for blob in blobs:
            #filename = blob.name.replace('/', '_') 
            result_path = blob.name.replace(prefix,"")
            folder = Path("results/google/"+target_path).parents[0]
            print(folder, target_path)
            os.makedirs(folder, exist_ok=True)
            blob.download_to_filename("results/google/"+target_path)  # Download

    def _find_dataset_id(self, name):
        response = self.automl_client.list_datasets(self.location_path, "")
        return [dataset.name for dataset in response if dataset.display_name == name]

    def prepare(self):
        storage_client = storage.Client()
        bucket = storage_client.bucket(self.bucket_name)
        if not bucket.exists():
            print(f"create new bucket {self.bucket_name} since it did not exist")
            bucket.create(location=self.region)

        self.bucket = bucket

    def upload_data(self, src_path: str, target_path: str):
        existing_dataset = self.get_dataset(self.project_name)
        if existing_dataset is not None:
            self.automl_client.delete_dataset(existing_dataset.name)
        self._upload_blob(src_path, target_path)
        self._create_dataset(target_path, display_name=self.project_name)

    def train_model(self, train_hours=1, exclude_columns=[]):
        create_model_response = self.tables_client.create_model(
            model_display_name=self.project_name,
            dataset=self.dataset,
            train_budget_milli_node_hours=train_hours * 1000,
            exclude_column_spec_names=exclude_columns,
        )

        print("Create model operation: {}".format(create_model_response.operation))
        self.last_submitted_automl_job = create_model_response
        return create_model_response

    def describe_model(self):
        pass

    def get_state(self):
        return self.last_submitted_automl_job.done()

    def get_dataset(self, dataset_name):
        try:
            dataset = self.tables_client.get_dataset(
                self.google_project_id, self.region, dataset_display_name=dataset_name
            )
            self.dataset = dataset
            return dataset
        except:
            print("Dataset does not exist")

    def describe_dataset(self, dataset=None):
        if dataset is None:
            dataset = self.dataset
        list_column_specs_response = self.tables_client.list_column_specs(
            dataset=dataset
        )
        column_specs = {s.display_name: s for s in list_column_specs_response}

        # Print Features and data_type.
        features = [
            (key, data_types.TypeCode.Name(value.data_type.type_code))
            for key, value in column_specs.items()
        ]
        print("Feature list:\n")
        for feature in features:
            print(feature[0], ":", feature[1])

    def download_model(self):
        if hasattr(self.last_submitted_automl_job, "name"):
            model_name = self.last_submitted_automl_job.name
        else:
            model_name = self.last_submitted_automl_job.result().name
        print("start export")
        export = self._export_model(model_name)
        export.result(300)
        self._download_model(path=export.metadata.export_model_details.output_info.gcs_output_directory)
        print("results will get downloaded to results/google")

    def get_most_recent_model(self):
        models = list(self.automl_client.list_models(self.location_path))
        return sorted(models, key=lambda s: s.update_time.seconds)[-1]

    def get_predictions(self, X_test):
        # payload = StringIO()
        # X_test.to_csv(payload, header=None, index=None)
        target_path = f"{self.project_name}/test.csv"
        self._upload_blob(X_test, target_path)
        model_display_name = self.project_name
        gcs_input_uris = [f'gs://{self.bucket_name}/{self.project_name}/test.csv']
        gcs_output_uri = f'gs://{self.bucket_name}/{self.project_name}/prediction.csv'

        response = self.tables_client.batch_predict(gcs_input_uris=gcs_input_uris,
                                        gcs_output_uri_prefix=gcs_output_uri,
                                        model_display_name=model_display_name)
        print("Making batch prediction... ")
        response.result()
        print("Batch prediction complete.\n{}".format(response.metadata))
        result_path = response.metadata.batch_predict_details.output_info.gcs_output_directory
        predictions_google = pd.read_csv(f"{result_path}/tables_1.csv")["salary_>50K_score"]

        index = predictions_google > 0.5
        predictions_google[index] = ">50K"
        predictions_google[~index] = "=<50K"
        return predictions_google


    def get_results(self, remote_run=None):
        if not self.get_state():
            raise Exception("Run is not completed")
        else:
            raise Exception("Implement the get_reults")

    def tear_down(self, force=False):
        self.bucket.delete(force=force)
