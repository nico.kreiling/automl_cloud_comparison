# Define IAM role
import re
from datetime import datetime as dt

import pandas as pd

from .auto_ml import AutoML
import boto3
from botocore.exceptions import ClientError
from sagemaker import get_execution_role

from io import StringIO  # python3; python2: BytesIO

bucket_name = "scieneers-nkreiling-automl"

target_metrics = dict()
target_metrics["BinaryClassification"] = 'F1'
target_metrics["MulticlassClassification"] = 'F1macro'
target_metrics["Regression"] = 'MSE'

class AwsAutoML(AutoML):
    def __init__(
        self,
        project_defaults: dict, 
        region,
        bucket_name,
        aws_role_arn: str = "arn:aws:iam::288518712839:role/sagemaker_nkreiling_full"
    ):
        project_defaults["region"] = region
        project_defaults["bucket_name"] = bucket_name
        super().__init__(**project_defaults)
        # self.storage_client = storage.Client(credentials=credentials, project=self.project_id)
        assert self.problem_type in target_metrics.keys()

        self.target_metric = target_metrics[self.problem_type]
        self.dataset = None
        self.aws_role_arn = aws_role_arn
        self.s3 = boto3.resource("s3")
        self.sm = boto3.client("sagemaker", region_name=self.region)
        self.last_submitted_automl_job = None

    def _create_bucket(self):
        boto3.client("s3", region_name=self.region).create_bucket(
            Bucket=self.bucket_name,
            CreateBucketConfiguration={"LocationConstraint": self.region},
        )

    def _write_df_to_s3(self, df: pd.DataFrame, target_path: str):
        csv_buffer = StringIO()
        df.to_csv(csv_buffer)
        return self.s3.Object(self.bucket_name, target_path).put(
            Body=csv_buffer.getvalue()
        )

    def _write_file_to_s3(self, local_path: str, target_path: str):
        self.s3.Object(self.bucket_name, target_path).put(Body=open(local_path, "rb"))

    def prepare(self):
        try:
            self._create_bucket()
        except ClientError as e:
            print(e)
            pass

    def upload_data(self, local_path: str, target_path: str):
        self._write_file_to_s3(local_path, target_path)

    def train_model(self, candidates_only=False):
        prefix = "sagemaker/" + self.project_name
        auto_ml_job_name = self.project_name + "-" + dt.now().strftime("%Y%m%d-%H%M%S")

        input_data_config = [
            {
                "DataSource": {
                    "S3DataSource": {
                        "S3DataType": "S3Prefix",
                        "S3Uri": "s3://{}/{}/train.csv".format(
                            self.bucket_name, self.project_name
                        ),
                    }
                },
                "TargetAttributeName": self.target_col,
            }
        ]

        output_data_config = {
            "S3OutputPath": "s3://{}/{}/output".format(self.bucket_name, prefix)
        }

        self.sm.create_auto_ml_job(
            AutoMLJobName=auto_ml_job_name,
            InputDataConfig=input_data_config,
            OutputDataConfig=output_data_config,
            ProblemType=self.problem_type,
            AutoMLJobObjective={
                'MetricName': self.target_metric
            },
            AutoMLJobConfig={
                'CompletionCriteria': {
                    'MaxAutoMLJobRuntimeInSeconds': self.max_training_time_min * 60
                },
            },
            RoleArn=self.aws_role_arn,
        )
        self.last_submitted_automl_job = auto_ml_job_name

    def get_state(self, auto_ml_job_name: str = None):
        if auto_ml_job_name is None:
            auto_ml_job_name = self.last_submitted_automl_job
        return self.sm.describe_auto_ml_job(AutoMLJobName=auto_ml_job_name)["AutoMLJobStatus"]
        
    def describe_model(self, auto_ml_job_name: str = None):
        if auto_ml_job_name is None:
            auto_ml_job_name = self.last_submitted_automl_job

        aws_automl_results = pd.DataFrame(self.sm.list_candidates_for_auto_ml_job(AutoMLJobName=auto_ml_job_name, SortBy='FinalObjectiveMetricValue').get("Candidates"))
        aws_automl_results["f1"] = aws_automl_results["FinalAutoMLJobObjectiveMetric"].map(lambda s: s["Value"])
        return aws_automl_results[["CandidateName", "ObjectiveStatus", "CandidateStatus", "CreationTime", "EndTime", "f1"]]
        
    def get_results(self, auto_ml_job_name: str = None):
        if auto_ml_job_name is None:
            auto_ml_job_name = self.last_submitted_automl_job

        return self.sm.describe_auto_ml_job(AutoMLJobName=auto_ml_job_name)["AutoMLJobArtifacts"]["CandidateDefinitionNotebookLocation"]
        
    def download_notebook(self, auto_ml_job_name: str = None):
        if auto_ml_job_name is None:
            auto_ml_job_name = self.last_submitted_automl_job

        url = self.sm.describe_auto_ml_job(AutoMLJobName=auto_ml_job_name)["AutoMLJobArtifacts"]["CandidateDefinitionNotebookLocation"].replace("s3://","")
        self.s3.Object(url.partition("/")[0], url.partition("/")[2]).download_file("SageMakerAutopilotCandidateDefinitionNotebook.ipynb")
        return "downloaded SageMakerAutopilotCandidateDefinitionNotebook.ipynb"

    def deploy(self):
        print("do this from the online notebook")
        # pipeline_model.deploy(initial_instance_count=1,
        #               instance_type='ml.t2.medium',
        #               endpoint_name=pipeline_model.name,
        #               wait=True)

    def get_predictions(self, sagemaker_endpoint, X_test):
        payload = StringIO()
        X_test.to_csv(payload, header=None, index=None)
        client = boto3.client('sagemaker-runtime', region_name=self.region)
        response = client.invoke_endpoint(
            EndpointName=sagemaker_endpoint,
            Body=payload.getvalue(),
            ContentType='text/csv')
        return response.get("Body").read().decode('utf-8').split("\n")

    def tear_down(self):
        #sm.list_notebook_instances()
        [sm.delete_model(ModelName=m["ModelName"]) for m in sm.list_models(MaxResults=100)["Models"]]
        pass